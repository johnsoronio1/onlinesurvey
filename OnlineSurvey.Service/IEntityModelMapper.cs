﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineSurvey.Service
{
    public interface IEntityModelMapper<TEntity, TModel>
             where TEntity : class, new()
             where TModel : class, new()
    {
        TModel EntityToModel(TEntity source);
        TEntity ModelToEntity(TModel source, TEntity destination);
        TEntity ModelToEntity(TModel source);

        List<TModel> EntityToModel(List<TEntity> source);
        List<TEntity> ModelToEntity(List<TModel> source);
    }
}
