﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Models;
using OnlineSurvey.Repository;
using OnlineSurvey.Repository.Interface;
using OnlineSurvey.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OnlineSurvey.Service
{
    public class QuestionService : IQuestionService
    {
        private readonly IEntityModelMapper<Question, QuestionViewModel> _mapper;
        private readonly QuestionRepository _questionRepository;
        private readonly SurveyRepository _surveyRepository;

        public QuestionService(IUnitOfWork unitOfWork)
        {
            _mapper = new EntityModelMapper<Question, QuestionViewModel>();
            _questionRepository = new QuestionRepository(unitOfWork);
            _surveyRepository = new SurveyRepository(unitOfWork);
        }
        public QuestionViewModel GetById(int id)
        {
            var question = _questionRepository.GetQuestion(id);
            var survey = _surveyRepository.Get(question.Survey.Id);
            var viewmodel = _mapper.EntityToModel(question);

            viewmodel.SurveyId = survey.Id;
            viewmodel.SurveyTitle = survey.Title;

            return viewmodel;
        }
        public QuestionViewModel Get(Expression<Func<Question, bool>> expression)
        {
            var entity = _questionRepository.Where(expression).SingleOrDefault();
            var viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public List<QuestionViewModel> List()
        {
            var entities = _questionRepository.GetQuestions();
            var viewmodels = (from item in entities
                select new QuestionViewModel()
                {
                    Id = item.Id,
                    DisplayText = item.DisplayText,
                    SurveyId = item.Survey.Id,
                    SurveyTitle = (item.Survey == null) ? string.Empty : item.Survey.Title,
                    Description = item.Description,
                    IsRequired = item.IsRequired,
                    RequiredMessage = item.RequiredMessage,
                    MinIntValue = item.MinIntValue,
                    MaxIntValue = item.MaxIntValue,
                    MinDecimalValue = item.MinDecimalValue,
                    MaxDecimalValue = item.MaxDecimalValue,
                    DateCreated = item.DateCreated,
                    DateCreatedBy = item.DateCreatedBy,
                    DateUpdated = item.DateUpdated,
                    DateUpdatedBy = item.DateUpdatedBy
                })
                .ToList();

            return viewmodels;
        }
        public List<QuestionViewModel> GetBySurveyId(int id)
        {
            var entities = _questionRepository.GetQuestions();
            var viewmodels = (from item in entities
                where item.Survey.Id == id
                select new QuestionViewModel()
                {
                    Id = item.Id,
                    DisplayText = item.DisplayText,
                    SurveyId = item.Survey.Id,
                    SurveyTitle = (item.Survey == null) ? string.Empty : item.Survey.Title,
                    Description = item.Description,
                    IsRequired = item.IsRequired,
                    RequiredMessage = item.RequiredMessage,
                    MinIntValue = item.MinIntValue,
                    MaxIntValue = item.MaxIntValue,
                    MinDecimalValue = item.MinDecimalValue,
                    MaxDecimalValue = item.MaxDecimalValue,
                    DateCreated = item.DateCreated,
                    DateCreatedBy = item.DateCreatedBy,
                    DateUpdated = item.DateUpdated,
                    DateUpdatedBy = item.DateUpdatedBy
                })
                .ToList();

            return viewmodels;
        }
        public List<QuestionViewModel> List(Expression<Func<Question, bool>> expression)
        {
            var entities = _questionRepository.Where(expression).ToList();
            var viewmodels = _mapper.EntityToModel(entities);

            return viewmodels;
        }
        public QuestionViewModel Add(QuestionViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);

            entity.Survey = _surveyRepository.GetWithTracking(viewmodel.SurveyId);
            entity = _questionRepository.Add(entity);

            viewmodel = _mapper.EntityToModel(entity);
            viewmodel.SurveyId = entity.Survey.Id;
            viewmodel.SurveyTitle = entity.Survey.Title;

            return viewmodel;
        }
        public void Update(QuestionViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);
            _questionRepository.Update(entity);
        }
        public void Delete(QuestionViewModel viewmodel)
        {
            var entity = _questionRepository.GetWithTracking(viewmodel.Id);

            _questionRepository.Delete(entity);
        }
        public bool Any(Expression<Func<Question, bool>> expression)
        {
            return _questionRepository.Where(expression).Any();
        }
        public PaginatedList<QuestionViewModel> PaginatedList(PaginatedModel model)
        {
            var query = _questionRepository.AsQueryable();

            if (!string.IsNullOrEmpty(model.SortField) && !string.IsNullOrEmpty(model.SortBy))
            {
                switch (model.SortField)
                {
                    case "title":
                        if (model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.Survey.Title);
                        else
                            query = query.OrderBy(m => m.Survey.Title);
                        break;
                    case "displayText":
                        if (model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.DisplayText);
                        else
                            query = query.OrderBy(m => m.DisplayText);
                        break;
                    case "description":
                        if (model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.Description);
                        else
                            query = query.OrderBy(m => m.Description);
                        break;
                    case "isRequired":
                        if (model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.IsRequired);
                        else
                            query = query.OrderBy(m => m.IsRequired);
                        break;
                    case "requiredMessage":
                        if (model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.RequiredMessage);
                        else
                            query = query.OrderBy(m => m.RequiredMessage);
                        break;
                    default:
                        query = query.OrderBy(m => m.Survey.Title);
                        break;
                }
            }
            else
                query = query.OrderBy(m => m.DateCreated);

            if (model.Filters != null && model.Filters.Any())
            {
                foreach (var filter in model.Filters)
                {
                    if (!string.IsNullOrEmpty(filter.Value))
                    {
                        switch (filter.Field)
                        {
                            case "surveyId":
                                int surveyId = Convert.ToInt32(filter.Value);
                                query = query.Where(m => m.Survey.Id == surveyId);
                                break;
                            case "title":
                                query = query.Where(m => m.Survey.Title.ToLower().Contains(filter.Value.ToLower()));
                                break;
                            case "displayText":
                                query = query.Where(m => m.DisplayText.ToLower().Contains(filter.Value.ToLower()));
                                break;
                            case "description":
                                query = query.Where(m => m.Description.ToLower().Contains(filter.Value.ToLower()));
                                break;
                            case "isRequired":
                                query = query.Where(m => m.IsRequired.Equals(Convert.ToBoolean(filter.Value)));
                                break;
                            case "requiredMessage":
                                query = query.Where(m => m.RequiredMessage.ToLower().Contains(filter.Value.ToLower()));
                                break;
                        }
                    }
                }
            }

            var projection = query
                .Include(m => m.Survey)
                .Select(m =>
                new QuestionViewModel()
                {
                    Id = m.Id,
                    SurveyId = m.Survey.Id,
                    SurveyTitle = m.Survey.Title,
                    DisplayText = m.DisplayText,
                    Description = m.Description,
                    IsRequired = m.IsRequired,
                    RequiredMessage = m.RequiredMessage,
                    MinIntValue = m.MinIntValue,
                    MaxIntValue = m.MaxIntValue,
                    MinDecimalValue = m.MinDecimalValue,
                    MaxDecimalValue = m.MaxDecimalValue,
                    DateCreated = m.DateCreated,
                    DateCreatedBy = m.DateCreatedBy,
                    DateUpdated = m.DateUpdated,
                    DateUpdatedBy = m.DateUpdatedBy
                }
            )
            .AsNoTracking();

            var paged = projection.ToPaginatedList(model.PageNumber, model.PageSize);

            return paged;
        }
    }
}
