﻿using OnlineSurvey.Entities;
using OnlineSurvey.Models;
using OnlineSurvey.Repository;
using OnlineSurvey.Repository.Interface;
using OnlineSurvey.Service;
using OnlineSurvey.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OnlineSurvey.Service
{
    public class UserService : IUserService
    {
        private readonly IEntityModelMapper<LoginUser, LoginUserViewModel> _mapper;
        private readonly IGenericRepository<LoginUser> _repository;

        public UserService(IUnitOfWork unitOfWork)
        {
            _repository = new GenericRepository<LoginUser>(unitOfWork);
            _mapper = new EntityModelMapper<LoginUser, LoginUserViewModel>();
        }

        public LoginUserViewModel GetById(int id)
        {
            var entity = _repository.Get(id);
            var viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public LoginUserViewModel Get(Expression<Func<LoginUser, bool>> expression)
        {
            var entity = _repository.Where(expression).SingleOrDefault();
            var viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public List<LoginUserViewModel> List()
        {
            var entities = _repository.List();
            var viewmodels = _mapper.EntityToModel(entities);

            return viewmodels;
        }
        public List<LoginUserViewModel> List(Expression<Func<LoginUser, bool>> expression)
        {
            var entities = _repository.Where(expression).ToList();
            var viewmodels = _mapper.EntityToModel(entities);

            return viewmodels;
        }
        public LoginUserViewModel Add(LoginUserViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);

            entity = _repository.Add(entity);

            viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public void Update(LoginUserViewModel viewmodel)
        {
            var entity = _repository.Get(viewmodel.Id);
            entity = _mapper.ModelToEntity(viewmodel, entity);

            _repository.Update(entity);
        }
        public void Delete(LoginUserViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);

            _repository.Delete(entity);
        }

        public bool Any(Expression<Func<LoginUser, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public PaginatedList<LoginUserViewModel> PaginatedList(PaginatedModel model)
        {
            throw new NotImplementedException();
        }
    }
}
