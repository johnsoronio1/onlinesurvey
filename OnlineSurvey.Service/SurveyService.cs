﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Models;
using OnlineSurvey.Repository;
using OnlineSurvey.Repository.Interface;
using OnlineSurvey.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OnlineSurvey.Service
{
    public class SurveyService : ISurveyService
    {
        private readonly IEntityModelMapper<Survey, SurveyViewModel> _mapper;
        private readonly IGenericRepository<Survey> _repository;

        public SurveyService(IUnitOfWork unitOfWork) {
            _repository = new GenericRepository<Survey>(unitOfWork);
            _mapper = new EntityModelMapper<Survey, SurveyViewModel>();
        }
        public SurveyViewModel GetById(int id)
        {
            var entity = _repository.Get(id);
            var viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public SurveyViewModel Get(Expression<Func<Survey, bool>> expression)
        {
            var entity = _repository.Where(expression).SingleOrDefault();
            var viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public List<SurveyViewModel> List()
        {
            var entities = _repository.List();
            var viewmodels = _mapper.EntityToModel(entities.ToList());

            return viewmodels;
        }
        public List<SurveyViewModel> List(Expression<Func<Survey, bool>> expression)
        {
            var entities = _repository.Where(expression).ToList();
            var viewmodels = _mapper.EntityToModel(entities);

            return viewmodels;
        }
        public SurveyViewModel Add(SurveyViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);
            entity.DateCreated = DateTime.Now;

            entity = _repository.Add(entity);

            viewmodel = _mapper.EntityToModel(entity);

            return viewmodel;
        }
        public void Update(SurveyViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);
            entity.DateUpdated = DateTime.Now;

            _repository.Update(entity);
        }
        public void Delete(SurveyViewModel viewmodel)
        {
            var entity = _mapper.ModelToEntity(viewmodel);

            _repository.Delete(entity);
        }
        public bool Any(Expression<Func<Survey, bool>> expression)
        {
            return _repository.Where(expression).Any();
        }

        public PaginatedList<SurveyViewModel> PaginatedList(PaginatedModel model)
        {
            var query = _repository.AsQueryable();

            if (!string.IsNullOrEmpty(model.SortField) && !string.IsNullOrEmpty(model.SortBy))
            {
                switch (model.SortField)
                {
                    case "title":
                        if(model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.Title);
                        else
                            query = query.OrderBy(m => m.Title);
                        break;
                    case "description":
                        if (model.SortBy.Equals("descend"))
                            query = query.OrderByDescending(m => m.Description);
                        else
                            query = query.OrderBy(m => m.Description);
                        break;
                }
            }
            else
                query = query.OrderBy(m => m.DateCreated);

            if (model.Filters != null && model.Filters.Any()) 
            {
                foreach (var filter in model.Filters) 
                {
                    if (!string.IsNullOrEmpty(filter.Value))
                    {
                        switch (filter.Field)
                        {
                            case "title":
                                query = query.Where(m => m.Title.ToLower().Contains(filter.Value.ToLower()));
                                break;
                            case "description":
                                query = query.Where(m => m.Description.ToLower().Contains(filter.Value.ToLower()));
                                break;
                        }
                    }
                }
            }

            var projection = query.Select(m => 
                new SurveyViewModel() {
                    Id = m.Id,
                    Title = m.Title,
                    Description = m.Description,
                    DatePublished = m.DatePublished,
                    DateCreated = m.DateCreated,
                    DateCreatedBy = m.DateCreatedBy,
                    DateUpdated = m.DateUpdated,
                    DateUpdatedBy = m.DateUpdatedBy
                }
            )
            .AsNoTracking();

            var paged = projection.ToPaginatedList(model.PageNumber, model.PageSize);

            return paged;
        }
    }
}
