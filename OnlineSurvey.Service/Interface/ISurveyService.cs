﻿using OnlineSurvey.Entities;
using OnlineSurvey.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineSurvey.Service.Interface
{
    public interface ISurveyService : IGenericService<Survey, SurveyViewModel>
    {

    }
}
