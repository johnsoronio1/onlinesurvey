﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace OnlineSurvey.Service
{
    public class EntityModelMapper<TEntity, TModel> : IEntityModelMapper<TEntity, TModel>
             where TEntity : class, new()
             where TModel : class, new()
    {
        protected readonly MapperConfiguration _config;
        protected readonly IMapper _mapper;

        public EntityModelMapper()
        {
            this._config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                cfg.CreateMap<TModel, TEntity>();
                cfg.CreateMap<TEntity, TModel>();
            });
            this._mapper = _config.CreateMapper();
        }

        public TModel EntityToModel(TEntity source)
        {
            return _mapper.Map<TEntity, TModel>(source);
        }

        public TEntity ModelToEntity(TModel source, TEntity destination)
        {
            return (TEntity)_mapper.Map(source, destination, typeof(TModel), typeof(TEntity));
        }

        public TEntity ModelToEntity(TModel source)
        {
            var destination = new TEntity();
            return (TEntity)_mapper.Map(source, destination, typeof(TModel), typeof(TEntity));
        }

        public List<TModel> EntityToModel(List<TEntity> source)
        {
            return _mapper.Map<List<TEntity>, List<TModel>>(source);
        }

        public List<TEntity> ModelToEntity(List<TModel> source)
        {
            return _mapper.Map<List<TModel>, List<TEntity>>(source);
        }
    }
}
