﻿using OnlineSurvey.Models;
using OnlineSurvey.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace OnlineSurvey.Service
{
    public interface IGenericService<TEntity, TModel>
             where TEntity : class, new()
             where TModel : class, new()
    {
        PaginatedList<TModel> PaginatedList(PaginatedModel model);
        TModel Get(Expression<Func<TEntity, bool>> expression);
        TModel GetById(int id);
        List<TModel> List();
        List<TModel> List(Expression<Func<TEntity, bool>> expression);
        TModel Add(TModel viewmodel);
        void Update(TModel viewmodel);
        void Delete(TModel viewmodel);
        bool Any(Expression<Func<TEntity, bool>> expression);
    }
}

