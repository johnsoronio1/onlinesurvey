﻿using OnlineSurvey.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace OnlineSurvey.Repository
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class QueryableExtensions
    {
        public static PaginatedList<T> ToPaginatedList<T>(this IQueryable<T> query, int page, int pageSize) where T : class
        {
            var result = new PaginatedList<T>();
            result.PageNumber = page;
            result.PageSize = pageSize;
            result.TotalRows = query.Count();

            var pageCount = (double)result.TotalRows / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }
    }
}
