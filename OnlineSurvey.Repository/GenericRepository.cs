﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Models;
using OnlineSurvey.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace OnlineSurvey.Repository
{
    public class GenericRepository<TEntity> : GenericRepository<TEntity, int>, IGenericRepository<TEntity>
      where TEntity : class, IBaseEntity<int>
    {
        public GenericRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }

    public class GenericRepository<TEntity, TId> : IGenericRepository<TEntity, TId>
        where TEntity : class, IBaseEntity<TId>
        where TId : IComparable
    {
        internal IUnitOfWork unitOfWork;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.dbSet = this.unitOfWork.Context.Set<TEntity>();
        }
        public TEntity Get(TId id)
        {
            IQueryable<TEntity> entities = AsQueryable();
            TEntity entity = Filter<TId>(entities, x => x.Id, id).AsNoTracking().FirstOrDefault();
            return entity;
        }
        public TEntity GetWithTracking(TId id)
        {
            IQueryable<TEntity> entities = AsQueryable();
            TEntity entity = Filter<TId>(entities, x => x.Id, id).FirstOrDefault();
            return entity;
        }
        public IQueryable<TEntity> AsQueryable()
        {
            return dbSet.AsQueryable();
        }
        public List<TEntity> List()
        {
            return dbSet.ToList();
        }
        public TEntity Add(TEntity entity)
        {
            dbSet.Add(entity);
            unitOfWork.SaveChanges();

            return entity;
        }
        public void Delete(TEntity entity)
        {
            dbSet.Remove(entity);
            unitOfWork.SaveChanges();
        }
        public void Update(TEntity entity)
        {
            unitOfWork.Context.Entry(entity).State = EntityState.Modified;
            unitOfWork.SaveChanges();
        }
        public IQueryable<TEntity> Where(Expression<Func<TEntity, bool>> where)
        {
            return dbSet.Where(where);
        }

        public PaginatedList<TEntity> Paginate(int pageIndex, int pageSize)
        {
            PaginatedList<TEntity> paginatedList = Paginate(pageIndex, pageSize);
            return paginatedList;
        }

        public PaginatedList<TEntity> Paginate(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector)
        {
            return Paginate(pageIndex, pageSize, keySelector, null);
        }

        public PaginatedList<TEntity> Paginate(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            PaginatedList<TEntity> paginatedList = Paginate(
                pageIndex, pageSize, keySelector, predicate, OrderByType.Ascending, includeProperties);

            return paginatedList;
        }

        public PaginatedList<TEntity> PaginateDescending(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector)
        {
            return PaginateDescending(pageIndex, pageSize, keySelector, null);
        }

        public PaginatedList<TEntity> PaginateDescending(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            PaginatedList<TEntity> paginatedList = Paginate(
                pageIndex, pageSize, keySelector, predicate, OrderByType.Descending, includeProperties);

            return paginatedList;
        }

        private PaginatedList<TEntity> Paginate(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderByType orderByType, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable =
                (orderByType == OrderByType.Ascending)
                    ? GetAllIncluding(includeProperties).OrderBy(keySelector)
                    : GetAllIncluding(includeProperties).OrderByDescending(keySelector);

            queryable = (predicate != null) ? queryable.Where(predicate) : queryable;
            PaginatedList<TEntity> paginatedList = queryable.ToPaginatedList(pageIndex, pageSize);

            return paginatedList;
        }

        public IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable = AsQueryable();
            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include(includeProperty);
            }

            return queryable;
        }

        private IQueryable<TEntity> Filter<TProperty>(IQueryable<TEntity> dbSet,
           Expression<Func<TEntity, TProperty>> property, TProperty value)
           where TProperty : IComparable
        {

            var memberExpression = property.Body as MemberExpression;
            if (memberExpression == null || !(memberExpression.Member is PropertyInfo))
            {

                throw new ArgumentException("Property expected", "property");
            }

            Expression left = property.Body;
            Expression right = Expression.Constant(value, typeof(TProperty));
            Expression searchExpression = Expression.Equal(left, right);
            Expression<Func<TEntity, bool>> lambda = Expression.Lambda<Func<TEntity, bool>>(
                searchExpression, new ParameterExpression[] { property.Parameters.Single() });

            return dbSet.Where(lambda);
        }
        public enum OrderByType
        {
            Ascending,
            Descending
        }
    }
}
