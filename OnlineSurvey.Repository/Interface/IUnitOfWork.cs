﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Entities.Identity;
using System;
using System.Threading.Tasks;

namespace OnlineSurvey.Repository.Interface
{
    public interface IUnitOfWork
    {
        ApplicationDbContext Context { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void Rollback();
        void Dispose();
    }
}