﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace OnlineSurvey.Repository.Interface
{
    public interface IGenericRepository<TEntity> : IGenericRepository<TEntity, int>
        where TEntity : class, IBaseEntity<int>
    {

    }

    public interface IGenericRepository<TEntity, TId>
        where TEntity : class, IBaseEntity<TId>
        where TId : IComparable
    {
        TEntity Get(TId id);
        TEntity GetWithTracking(TId id);

        List<TEntity> List();
        IQueryable<TEntity> AsQueryable();
        IQueryable<TEntity> Where(Expression<Func<TEntity, Boolean>> where);
        TEntity Add(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);

        PaginatedList<TEntity> Paginate(int pageIndex, int pageSize);
        PaginatedList<TEntity> Paginate(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector);
        PaginatedList<TEntity> Paginate(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
        PaginatedList<TEntity> PaginateDescending(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector);
        PaginatedList<TEntity> PaginateDescending(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
    }
}
