﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace OnlineSurvey.Repository
{
    public class SurveyRepository : GenericRepository<Survey, int>
    {
        private readonly IUnitOfWork _unitOfWork;
        public SurveyRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
