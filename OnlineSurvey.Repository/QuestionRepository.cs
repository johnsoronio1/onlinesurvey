﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities;
using OnlineSurvey.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace OnlineSurvey.Repository
{
    public class QuestionRepository : GenericRepository<Question, int>
    {
        private readonly IUnitOfWork _unitOfWork;
        public QuestionRepository(IUnitOfWork unitOfWork) : base(unitOfWork) {
            this._unitOfWork = unitOfWork;
        }

        public List<Question> GetQuestions() {
            var entities = new List<Question>();

            entities = _unitOfWork.Context.Set<Question>()
                .AsNoTracking()
                .Include(m => m.Survey)
                .ToList();

            return entities;
        }

        public Question GetQuestion(int id) {
            var entity = new Question();

            entity = _unitOfWork.Context.Set<Question>()
                .AsNoTracking()
                .Include(m => m.Survey)
                .Where(m => m.Id == id)
                .FirstOrDefault();

            return entity;
        }
    }
}
