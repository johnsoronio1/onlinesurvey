## **Welcome**
**Default Login Users**
* admin `tempP@ss123`
* user `tempP@ss123`

**OnlineSurvey.AuthServer**
* Running at http://localhost:5000

**OnlineSurvey.Api**
* Running at http://localhost:5001

**OnlineSurvey.Client**
* Running at http://localhost:4200

## **Installation Instructions**
**Download and Install the following applications or tools:**
1. Git
2. Git Extensions
3. Node
4. Visual Studio Code
5. Visual Studio 2019 Community
6. Postman
## **Getting Started**

**1. Clone the Repository:**
* Open the Git Extensions
* Copy and paste the gitlab link `https://gitlab.com/johnsoronio1/onlinesurvey` repository
* Click `Clone Repository`

**2. Get the latest copy**
* Run `git pull` in the console tab (Git Extensions)

**3. Open the ClientApp**
* Open the Visual Studio Code
* Click `File > Open folder...`
* Locate the ClientApp folder in the `OnlineSurvey.Client` project
* Click select

**4. NPM Install**
* Click `Terminal > New Terminal` (Visual Studio Code)
* Run `npm install`

**5. Open the Project Solution**
* Open the OnlineSurvey solution
* Run `Build Solution`
* `Start Debugging`

**6. Run Entity Framework Migrations**
* Set `OnlineSurvey.Entities` as default project
* Open `Package Manager Console`
* Set default project: `OnlineSurvey.Entities`
* Run `Update-Database -Context ApplicationDbContext`, then `Enter`
* Run `Update-Database -Context PersistedGrantDbContext`, then `Enter`

