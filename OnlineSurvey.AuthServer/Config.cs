﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using OnlineSurvey.Entities.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSurvey.AuthServer
{
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("apiresource") {
                    UserClaims = {
                        JwtClaimTypes.SessionId,
                        JwtClaimTypes.Name,
                        JwtClaimTypes.Email,
                        JwtClaimTypes.PhoneNumber,
                        JwtClaimTypes.Role,
                        JwtClaimTypes.Subject,
                        ClaimConstants.Permission
                    }
                }
            };
        }

        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            var data = new[]
            {
                new Client
                {
                    ClientId = "client",
                    ClientName = "Client Credentials Client",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("511536EF-F270-4058-80CA-1C89C192F69A".Sha256()) },

                    AllowedScopes = { "openid", "profile", configuration["IdentityServer4:ApiResource"] }
                },
                new Client {
                    RequireConsent = false,
                    ClientId = configuration["IdentityServer4:ClientId"],
                    ClientSecrets = { new Secret(configuration["IdentityServer4:ClientSecret"].Sha256()) },
                    ClientName = "Angular SPA",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes = { "openid", "profile", configuration["IdentityServer4:ApiResource"] },
                    RedirectUris = { configuration["IdentityServer4:ClientCallbackUrl"] },
                    PostLogoutRedirectUris = { configuration["IdentityServer4:ClientUrl"] },
                    AllowedCorsOrigins = { configuration["IdentityServer4:ClientUrl"] },
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenLifetime = 3600
                },
                new Client {
                    ClientId = "angular_spa",
                    ClientName = "Angular 4 Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequirePkce = true,
                    RequireClientSecret = false,
                    AllowedScopes = new List<string> {"openid", "profile", configuration["IdentityServer4:ApiResource"] },
                    RedirectUris = new List<string> { configuration["IdentityServer4:ClientCallbackUrl"], configuration["IdentityServer4:ClientSilentRefreshUrl"] },
                    PostLogoutRedirectUris = new List<string> { configuration["IdentityServer4:ClientUrl"] },
                    AllowedCorsOrigins = new List<string> { configuration["IdentityServer4:ClientUrl"] },
                    AllowAccessTokensViaBrowser = true
                }
            };

            return data;
        }
    }
}
