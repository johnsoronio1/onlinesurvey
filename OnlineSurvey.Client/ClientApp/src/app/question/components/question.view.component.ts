import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../question.service';
import { QuestionModel } from '../question.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-question-view',
    templateUrl: './question.view.component.html',
    styleUrls: ['./question.view.component.scss']
})
export class QuestionViewComponent implements OnInit {

    public id: number;
    public question: QuestionModel;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: QuestionService,
        private spinner: NgxSpinnerService
    ){ }

    ngOnInit() {
        this.question = new QuestionModel();
        this.id = this.route.snapshot.params['id'];

        this.spinner.show();
        this.service.getbyId(this.id)
          .subscribe(result => {
              this.question = result;
              this.question.isRequired = (result.isRequired !== true ? false : true); 
              this.spinner.hide();
          }, error => console.log(error));
    }

    goToList() {
      this.router.navigate(['/question']);
    }
}
