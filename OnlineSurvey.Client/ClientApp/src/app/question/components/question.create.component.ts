import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuestionService } from '../question.service';
import { QuestionModel } from '../question.model';
import { SurveyModel } from '../../survey/survey.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-question-create',
    templateUrl: './question.create.component.html',
    styleUrls: ['./question.create.component.scss']
})
export class QuestionCreateComponent implements  OnInit {

    public question: QuestionModel;
    public surveys: Observable<SurveyModel[]>;
    public validateForm: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: QuestionService,
        private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() { 
        this.question = new QuestionModel();
        this.service.getSurveys().subscribe((result) => {
            this.surveys = result;
        }, error => console.log(error));

        this.validateForm = this.formBuilder.group({
            surveyId: [null, [Validators.required]],
            displayText: [null, [Validators.required]],
            description: [null, [Validators.required]],
            isRequired: [false],
            requiredMessage: [null],
            minIntValue: [null],
            maxIntValue: [null],
            minDecimalValue: [null],
            maxDecimalValue: [null]
        });
    }

    createQuestion() {
        this.spinner.show();
        this.service.create(this.question)
        .subscribe(result => {        
            this.question = new QuestionModel();
            this.goToList();
            this.spinner.hide();
        }, error => console.log(error));
    }

    goToList() {
        this.router.navigate(['/question']);
    }

    onSubmit(): void {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.invalid) {
            return;
        }

        this.createQuestion();
    }
}
