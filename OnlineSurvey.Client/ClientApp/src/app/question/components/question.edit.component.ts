import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuestionService } from '../question.service';
import { QuestionModel } from '../question.model';
import { SurveyModel } from '../../survey/survey.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-question-edit',
    templateUrl: './question.edit.component.html',
    styleUrls: ['./question.edit.component.scss']
})
export class QuestionEditComponent implements OnInit {

    public id: number;
    public question: QuestionModel;
    public surveys: Observable<SurveyModel[]>;
    public validateForm: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: QuestionService,
        private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.question = new QuestionModel();
        this.surveys = this.service.getSurveys();

        if(this.question.isRequired == null){
            this.question.isRequired == false;
        }
        
        this.id = this.route.snapshot.params['id'];

        this.spinner.show();
        this.service.getbyId(this.id)
          .subscribe(result => {
              this.question = result;
              this.validateForm.patchValue({
                surveyId: this.question.surveyId,
                displayText: this.question.displayText,
                description: this.question.description,
                isRequired: this.question.isRequired,
                requiredMessage: this.question.requiredMessage,
                minIntValue: this.question.minIntValue,
                maxIntValue: this.question.maxIntValue,
                minDecimalValue: this.question.minDecimalValue,
                maxDecimalValue: this.question.maxDecimalValue
              });

              this.spinner.hide();
          }, error => console.log(error));

          this.validateForm = this.formBuilder.group({
            surveyId: [null, [Validators.required]],
            displayText: [null, [Validators.required]],
            description: [null, [Validators.required]],
            isRequired: [false],
            requiredMessage: [null],
            minIntValue: [null],
            maxIntValue: [null],
            minDecimalValue: [null],
            maxDecimalValue: [null]
        });
    }

    updateQuestion() {
        this.spinner.show();
        this.service.update(this.id, this.question)
        .subscribe(result => {        
            this.question = new QuestionModel();
            this.spinner.hide();
            this.goToList();
        }, error => console.log(error));
    }

    goToList() {
        this.router.navigate(['/question']);
    }

    onSubmit() {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.invalid) {
            return;
        }

        this.updateQuestion();
    }
}
