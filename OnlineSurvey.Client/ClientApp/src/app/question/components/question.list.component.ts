import { Component, Input, OnInit  } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { QuestionService } from '../question.service';
import { QuestionModel } from '../question.model';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Pagination } from '../../shared/models/pagination';
import { PaginationFilters } from '../../shared/models/pagination.filters';

@Component({
  selector: 'app-question',
  templateUrl: './question.list.component.html',
  styleUrls: ['./question.list.component.scss']
})
export class QuestionListComponent implements OnInit {

    @Input() public surveyId?: number;
    public questions: Observable<QuestionModel[]>;
    public pagination: Pagination;
    public paginationFilters: PaginationFilters;
    public loading: Boolean;

    isAllDisplayDataChecked = false;
    isIndeterminate = false;
    listOfDisplayData: QuestionModel[] = [];
    listOfAllData: QuestionModel[] = [];
    mapOfCheckedId: { [key: number]: boolean } = {};

    constructor(
        private service: QuestionService, 
        private router: Router,
        private modalService: NzModalService,
        private notification: NzNotificationService
    ) { 
        this.pagination = new Pagination();
    }

    ngOnInit() {
        this.pagination.pageNumber = 1;
        this.pagination.pageSize = 10;
        this.pagination.totalRows = 1;
        this.pagination.sortField = '';
        this.pagination.sortBy = '';
        this.pagination.filters = [];

        console.log('surveyId: ', this.surveyId);

        this.getList();
    }

    getList(reset: boolean = false) {
        this.service.get(this.surveyId)
        .subscribe(result => {
            this.questions = result;
        }, error => console.log(error));

        if(reset) {
            this.pagination.pageNumber = 1;
        }

        if(this.surveyId != null && this.surveyId > 0) {
            this.pagination.filters.push({
                field: 'surveyId',
                value: this.surveyId
            });
        }

        this.loading = true;
        this.service.paginate(this.pagination).subscribe((result) => {
            this.pagination = result;
            this.listOfDisplayData = result.results;
            this.loading = false;
        }, error => console.log(error));
    }

    createQuestion(){
        this.router.navigate(['/question/create']);
    }

    questionDetails(id: number) {
        this.router.navigate(['/question/view', id]);
    }

    updateQuestion(id: number) {
        this.router.navigate(['/question/edit', id]);
    }

    showDeleteConfirmDialog(id: number, displayText: string) {
        this.modalService.confirm({
            nzTitle: 'Confirm Delete',
            nzContent: `Are you sure you want to delete this question - '${displayText}'?`, 
            nzOkText: 'Delete',
            nzOkType: 'danger',
            nzWidth: '600px',
            nzOnOk: () => {
                this.service.delete(id)
                .subscribe(result => {
                    this.getList();
                    this.notification.create(
                        'success',
                        'Request Completed Successfully',
                        'This is the content of the notification. This is the content of the notification. This is the content of the notification.'
                    );
                }, error => console.log(error));
            },
            nzCancelText: 'Cancel',
            nzOnCancel: () => console.log('Cancel')
        });
    }

    sortBy(sort: { key: string; value: string }): void {
        this.pagination.sortField = sort.key;
        this.pagination.sortBy = sort.value;

        this.getList();
    }

    currentPageDataChange($event: QuestionModel[]): void {
        this.listOfDisplayData = $event;
        this.refreshStatus();
    }
    
    refreshStatus(): void {
        this.isAllDisplayDataChecked = this.listOfDisplayData.every(item => this.mapOfCheckedId[item.id]);
        this.isIndeterminate =
        this.listOfDisplayData.some(item => this.mapOfCheckedId[item.id]) && !this.isAllDisplayDataChecked;
    }

    checkAll(value: boolean): void {
        this.listOfDisplayData.forEach(item => (this.mapOfCheckedId[item.id] = value));
        this.refreshStatus();
    }
}
