export class QuestionModel {
    id: number;
    surveyId: number;
    surveyTitle: string;
    displayText: string;
    isRequired?: boolean;
    description: string;
    requiredMessage: string;
    minIntValue?: number;
    maxIntValue?: number;
    minDecimalValue?: number;
    maxDecimalValue?: number;
    dateCreated: string;
    dateCreatedBy?: number;
    dateUpdated: string;
    dateUpdatedBy?: number;
}
