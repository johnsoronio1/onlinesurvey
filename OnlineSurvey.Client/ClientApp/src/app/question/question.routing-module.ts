import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionListComponent } from '../question/components/question.list.component';
import { QuestionCreateComponent } from '../question/components/question.create.component';
import { QuestionEditComponent } from '../question/components/question.edit.component';
import { QuestionViewComponent } from '../question/components/question.view.component';
import { AuthGuard } from '../core/authentication/auth.guard';
import { Shell } from './../shell/shell.service';

const routes: Routes = [
Shell.childRoutes([
    { path: 'question', component: QuestionListComponent, canActivate: [AuthGuard] },
    { path: 'question/create', component: QuestionCreateComponent, canActivate: [AuthGuard] },
    { path: 'question/edit/:id', component: QuestionEditComponent, canActivate: [AuthGuard] },
    { path: 'question/view/:id', component: QuestionViewComponent, canActivate: [AuthGuard] }, 
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class QuestionRoutingModule { }