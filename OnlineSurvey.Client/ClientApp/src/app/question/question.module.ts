import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule }   from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { QuestionListComponent } from './components/question.list.component';
import { QuestionCreateComponent } from './components/question.create.component';
import { QuestionEditComponent } from './components/question.edit.component';
import { QuestionViewComponent } from './components/question.view.component';
import { QuestionService } from './question.service';
import { QuestionRoutingModule } from './question.routing-module';
import { AuthService }  from '../core/authentication/auth.service';
import { DropdownSelectModule } from '../shared/dropdown-select/dropdown-select.module';

import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';

@NgModule({
  declarations: [
    QuestionListComponent,
    QuestionCreateComponent,
    QuestionEditComponent,
    QuestionViewComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    FormsModule,
    CommonModule,  
    SharedModule,

    QuestionRoutingModule,
    DropdownSelectModule,

    NzGridModule,
    NzButtonModule,
    NzCardModule,
    NzIconModule,
    NzTableModule,
    NzDividerModule,
    NzInputModule,
    NzFormModule,
    NzRadioModule,
    NzModalModule,
    NzNotificationModule,
    NzSelectModule
  ],
  exports:[QuestionListComponent],
  providers: [
    AuthService,
    QuestionService
  ]
})
export class QuestionModule { }
