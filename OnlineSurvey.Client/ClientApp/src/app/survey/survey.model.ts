export class SurveyModel {
    id: number;
    title: string;
    description: string;
    dateCreated: string;
    dateCreatedBy?: number;
    dateUpdated: string;
    dateUpdatedBy?: number;
}
