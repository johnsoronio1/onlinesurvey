import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SurveyListComponent } from '../survey/components/survey.list.component';
import { SurveyCreateComponent } from '../survey/components/survey.create.component';
import { SurveyEditComponent } from '../survey/components/survey.edit.component';
import { SurveyViewComponent } from '../survey/components/survey.view.component';
import { AuthGuard } from '../core/authentication/auth.guard';
import { Shell } from './../shell/shell.service';

const routes: Routes = [
Shell.childRoutes([
    { path: 'survey', component: SurveyListComponent, canActivate: [AuthGuard] },
    { path: 'survey/create', component: SurveyCreateComponent, canActivate: [AuthGuard] },
    { path: 'survey/edit/:id', component: SurveyEditComponent, canActivate: [AuthGuard] },
    { path: 'survey/view/:id', component: SurveyViewComponent, canActivate: [AuthGuard] }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SurveyRoutingModule { }