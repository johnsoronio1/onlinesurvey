import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { SurveyService } from '../survey.service';
import { SurveyModel } from '../survey.model';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Pagination } from '../../shared/models/pagination';
import { PaginationFilters } from '../../shared/models/pagination.filters';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.list.component.html',
  styleUrls: ['./survey.list.component.scss']
})
export class SurveyListComponent implements OnInit {

    public surveys: Observable<SurveyModel[]>;
    public pagination: Pagination;
    public loading: Boolean;

    isAllDisplayDataChecked = false;
    isIndeterminate = false;
    listOfDisplayData: SurveyModel[] = [];
    listOfAllData: SurveyModel[] = [];
    mapOfCheckedId: { [key: number]: boolean } = {};

    constructor(
        private service: SurveyService, 
        private router: Router,
        private modalService: NzModalService,
        private notification: NzNotificationService
    ) {
        this.pagination = new Pagination();
    }

    ngOnInit() {
        this.pagination.pageNumber = 1;
        this.pagination.pageSize = 10;
        this.pagination.totalRows = 1;
        this.pagination.sortField = '';
        this.pagination.sortBy = '';

        this.getList(true);
    }

    getList(reset: boolean = false): void {
        if(reset) {
            this.pagination.pageNumber = 1;
        }

        this.loading = true;
        this.service.paginate(this.pagination).subscribe((result) => {
            this.pagination = result;
            this.listOfDisplayData = result.results;
            this.loading = false;
        }, error => console.log(error));
    }

    createSurvey(){
        this.router.navigate(['/survey/create']);
    }

    surveyDetails(id: number) {
        this.router.navigate(['/survey/view', id]);
    }

    updateSurvey(id: number) {
        this.router.navigate(['/survey/edit', id]);
    }

    showDeleteConfirmDialog(id: number, title: string) {
        this.modalService.confirm({
            nzTitle: 'Confirm Delete',
            nzContent: `Are you sure you want to delete this survey - '${title}'?`, 
            nzOkText: 'Delete',
            nzOkType: 'danger',
            nzWidth: '600px',
            nzOnOk: () => {
                this.service.delete(id)
                .subscribe(result => {
                    this.getList();
                    this.notification.create(
                        'success',
                        'Request Completed Successfully',
                        'This is the content of the notification. This is the content of the notification. This is the content of the notification.'
                    );
                }, error => console.log(error));
            },
            nzCancelText: 'Cancel',
            nzOnCancel: () => console.log('Cancel')
        });
    }

    sortBy(sort: { key: string; value: string }): void {
        this.pagination.sortField = sort.key;
        this.pagination.sortBy = sort.value;

        this.getList();
    }

    currentPageDataChange($event: SurveyModel[]): void {
        this.listOfDisplayData = $event;
        this.refreshStatus();
    }
    
    refreshStatus(): void {
        this.isAllDisplayDataChecked = this.listOfDisplayData.every(item => this.mapOfCheckedId[item.id]);
        this.isIndeterminate =
        this.listOfDisplayData.some(item => this.mapOfCheckedId[item.id]) && !this.isAllDisplayDataChecked;
    }

    checkAll(value: boolean): void {
        this.listOfDisplayData.forEach(item => (this.mapOfCheckedId[item.id] = value));
        this.refreshStatus();
    }
}
