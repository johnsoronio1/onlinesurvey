import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from '../survey.service';
import { SurveyModel } from '../survey.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-survey-edit',
    templateUrl: './survey.edit.component.html',
    styleUrls: ['./survey.edit.component.scss']
})
export class SurveyEditComponent implements OnInit {

    public id: number;
    public survey: SurveyModel;
    public validateForm: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: SurveyService,
        private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit(): void {
        this.survey = new SurveyModel();
        this.id = this.route.snapshot.params['id'];

        this.spinner.show();
        this.service.getbyId(this.id)
        .subscribe(result => {
            this.survey = result;

            this.validateForm.patchValue({
                title: this.survey.title, 
                description: this.survey.description
            });

            this.spinner.hide();
        }, error => console.log(error));

        this.validateForm = this.formBuilder.group({
            title: [null, [Validators.required]],
            description: [null, [Validators.required]]
        });
    }

    updateSurvey() {
        this.spinner.show();
        this.service.update(this.id, this.survey)

        .subscribe(result => {        
            this.survey = new SurveyModel();
            this.spinner.hide();
            this.goToList();
        }, error => console.log(error));
    }

    goToList() {
        this.router.navigate(['/survey']);
    }

    onSubmit(): void {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.invalid) {
            return;
        }

        this.updateSurvey();
    }
}
