import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyService } from '../survey.service';
import { SurveyModel } from '../survey.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-survey-create',
    templateUrl: './survey.create.component.html',
    styleUrls: ['./survey.create.component.scss']
})
export class SurveyCreateComponent implements  OnInit {

    public survey: SurveyModel;
    public validateForm: FormGroup;

    constructor(
        private router: Router,
        private service: SurveyService,
        private spinner: NgxSpinnerService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() { 
        this.survey = new SurveyModel();

        this.validateForm = this.formBuilder.group({
            title: [null, [Validators.required]],
            description: [null, [Validators.required]]
        });
    }

    createSurvey() {
        this.spinner.show();
        this.service.create(this.survey)
        .subscribe(result => {        
            this.survey = new SurveyModel();
            this.spinner.hide();
            this.goToList();
        }, error => console.log(error));
    }

    goToList() {
        this.router.navigate(['/survey']);
    }

    onSubmit(): void {
        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.invalid) {
            return;
        }
        
        this.createSurvey();
    }
}
