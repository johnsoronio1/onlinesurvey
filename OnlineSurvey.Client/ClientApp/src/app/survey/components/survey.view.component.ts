import { Component, OnInit, Input  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from '../survey.service';
import { SurveyModel } from '../survey.model';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-survey-view',
    templateUrl: './survey.view.component.html',
    styleUrls: ['./survey.view.component.scss']
})
export class SurveyViewComponent implements OnInit {

    id: number;
    survey: SurveyModel;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: SurveyService,
        private spinner: NgxSpinnerService
    ){ }

    ngOnInit() {
        this.survey = new SurveyModel();
        this.id = this.route.snapshot.params['id'];

        this.spinner.show();
        this.service.getbyId(this.id)
        .subscribe(result => {
            this.survey = result;
            this.spinner.hide();
        }, error => console.log(error));
    }

    goToList() {
      this.router.navigate(['/survey']);
    }
}
