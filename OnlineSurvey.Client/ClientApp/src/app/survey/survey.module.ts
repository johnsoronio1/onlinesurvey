import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule }   from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SurveyListComponent } from './components/survey.list.component';
import { SurveyCreateComponent } from './components/survey.create.component';
import { SurveyEditComponent } from './components/survey.edit.component';
import { SurveyViewComponent } from './components/survey.view.component';
import { SurveyService }  from './survey.service';
import { QuestionModule } from '../question/question.module';
import { SurveyRoutingModule } from './survey.routing-module';
import { AuthService }  from '../core/authentication/auth.service';
import { DropdownSelectModule } from '../shared/dropdown-select/dropdown-select.module';

import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';

@NgModule({
  declarations: [
    SurveyListComponent,
    SurveyCreateComponent,
    SurveyEditComponent,
    SurveyViewComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
    FormsModule,
    CommonModule,  
    SharedModule,
    
    QuestionModule,
    SurveyRoutingModule,
    DropdownSelectModule,

    NzGridModule,
    NzButtonModule,
    NzCardModule,
    NzIconModule,
    NzTableModule,
    NzDividerModule,
    NzInputModule,
    NzFormModule,
    NzModalModule,
    NzNotificationModule
  ],
  providers: [
    AuthService,
    SurveyService
  ]
})
export class SurveyModule { }
