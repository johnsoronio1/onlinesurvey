import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from '../shared/config.service';
import { BaseService } from "../shared/base.service";
import { AuthService } from '../core/authentication/auth.service';
import { Pagination } from '../shared/models/pagination';

@Injectable({ providedIn: 'root' })
export class SurveyService extends BaseService {
    private httpOptions: any;
    constructor(private http: HttpClient, private configService: ConfigService, private authService: AuthService) { 
        super();

        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
                'Authorization': this.authService.authorizationHeaderValue
            })
        };
    }

    get(): Observable<any> {
        return this.http.get<any>(`${this.configService.resourceApiURI}/survey`, this.httpOptions);
    }

    paginate(paginate: Pagination): Observable<any> { 
        return this.http.post<any>(`${this.configService.resourceApiURI}/survey/paginate`, paginate, this.httpOptions);
    }

    getbyId(id: number): Observable<any> {
        return this.http.get<any>(`${this.configService.resourceApiURI}/survey/${id}`, this.httpOptions);
    }

    create(model: any): Observable<Object> {
        return this.http.post(`${this.configService.resourceApiURI}/survey`, model, this.httpOptions);
    }

    update(id: number, model: any): Observable<Object> {
        return this.http.put(`${this.configService.resourceApiURI}/survey/${id}`, model, this.httpOptions);
    }

    delete(id: number): Observable<any>{
        return this.http.delete(`${this.configService.resourceApiURI}/survey/${id}`, this.httpOptions);
    }
}
