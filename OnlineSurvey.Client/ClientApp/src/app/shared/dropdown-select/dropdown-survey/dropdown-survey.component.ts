import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { SurveyModel } from '../../../survey/survey.model';
import { SurveyService } from '../../../survey/survey.service';

@Component({
  selector: 'app-dropdown-survey',
  templateUrl: './dropdown-survey.component.html',
  styleUrls: ['./dropdown-survey.component.scss']
})
export class DropdownSurveyComponent implements OnInit {

    @Input() public id: string;
    @Input() public name: string;
    @Input() public label: string;
    @Input() public inputModel: string;
    @Output() public inputModelChange = new EventEmitter<string>();

    public surveys: Observable<SurveyModel[]>;

    constructor(
        private service: SurveyService, 
        private router: Router
    ) { }

    ngOnInit() {
        this.getList();
    }

    getList() {
        this.surveys = this.service.get();
    }
}
