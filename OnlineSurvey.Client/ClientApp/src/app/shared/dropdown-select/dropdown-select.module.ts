import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DropdownSurveyComponent } from '../dropdown-select/dropdown-survey/dropdown-survey.component';

@NgModule({
  declarations: [
    DropdownSurveyComponent
  ],
  exports:[ 
    DropdownSurveyComponent
  ],
  imports: [
      BrowserModule,
      FormsModule
  ],
  providers: []
})
export class DropdownSelectModule { }
