export class Pagination {
    pageNumber: number;
    pageCount: number;
    pageSize: number;
    
    sortField: string;
    sortBy: string;
    filters: any;

    totalRows: number;
    hasPrevious: boolean;
    hasNext: boolean;
    firstRowOnPage: number;
    lastRowOnPage: number;
    results: any;
}
