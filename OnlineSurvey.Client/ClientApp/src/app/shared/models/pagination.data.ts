export class PaginationData {
    pageNumber: number;
    pageCount: number;
    pageSize: number;
    totalRows: number;
    hasPrevious: boolean;
    hasNext: boolean;
    firstRowOnPage: number;
    lastRowOnPage: number;
    results: any;
}
