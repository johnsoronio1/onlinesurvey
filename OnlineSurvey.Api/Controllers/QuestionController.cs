﻿using System;
using System.Collections.Generic;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineSurvey.Api.Common;
using OnlineSurvey.Models;
using OnlineSurvey.Service.Interface;

namespace OnlineSurvey.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    public class QuestionController : ControllerBase
    {
        private readonly IQuestionService _service;

        public QuestionController(IQuestionService service)
        {
            _service = service;
        }

        // GET: api/Question
        [HttpGet]
        public ActionResult<List<QuestionViewModel>> GetQuestions()
        {
            return _service.List();
        }

        [HttpPost]
        [Route("Paginate")]
        public ActionResult<PaginatedList<QuestionViewModel>> Paginate(PaginatedModel model)
        {
            return _service.PaginatedList(model);
        }

        // GET: api/Question/5
        [HttpGet("{id}")]
        public ActionResult<QuestionViewModel> GetQuestion(int id)
        {
            var question = _service.GetById(id);

            if (question == null)
            {
                return NotFound();
            }

            return question;
        }

        // GET: api/question/survey/5
        [HttpGet]
        [Route("survey/{id}")]
        public ActionResult<List<QuestionViewModel>> GetbySurvey(int id)
        {
            return _service.GetBySurveyId(id);
        }

        // PUT: api/Question/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public IActionResult PutQuestion(int id, QuestionViewModel viewmodel)
        {
            if (id != viewmodel.Id)
            {
                return BadRequest();
            }

            try
            {
                _service.Update(viewmodel);
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();
        }

        // POST: api/Question
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public ActionResult<QuestionViewModel> PostQuestion(QuestionViewModel viewmodel)
        {
            viewmodel = _service.Add(viewmodel);

            return viewmodel;
        }

        // DELETE: api/Question/5
        [HttpDelete("{id}")]
        public ActionResult<QuestionViewModel> DeleteQuestion(int id)
        {
            var viewmodel = _service.GetById(id);
            if (viewmodel == null)
            {
                return NotFound();
            }

            _service.Delete(viewmodel);

            return viewmodel;
        }
    }
}
