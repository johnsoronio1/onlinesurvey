﻿using System;
using System.Collections.Generic;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineSurvey.Api.Common;
using OnlineSurvey.Models;
using OnlineSurvey.Service.Interface;

namespace OnlineSurvey.Api.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    public class SurveyController : ControllerBase
    {
        private readonly ISurveyService _service;

        public SurveyController(ISurveyService service)
        {
            _service = service;
        }

        // GET: api/Survey
        [HttpGet]
        public ActionResult<List<SurveyViewModel>> GetSurveys()
        {
            return _service.List();
        }

        [HttpPost]
        [Route("paginate")]
        public ActionResult<PaginatedList<SurveyViewModel>> Paginate(PaginatedModel model)
        {
            return _service.PaginatedList(model);
        }

        // GET: api/Survey/5
        [HttpGet("{id}")]
        public ActionResult<SurveyViewModel> GetSurvey(int id)
        {
            var survey = _service.GetById(id);

            if (survey == null)
            {
                return NotFound();
            }

            return survey;
        }

        // PUT: api/Survey/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public IActionResult PutSurvey(int id, SurveyViewModel viewmodel)
        {
            if (id != viewmodel.Id)
            {
                return BadRequest();
            }

            try
            {
                _service.Update(viewmodel);
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();
        }

        // POST: api/Survey
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public ActionResult<SurveyViewModel> PostSurvey(SurveyViewModel viewmodel)
        {
            viewmodel = _service.Add(viewmodel);

            return viewmodel;
        }

        // DELETE: api/Survey/5
        [HttpDelete("{id}")]
        public ActionResult<SurveyViewModel> DeleteSurvey(int id)
        {
            var viewmodel = _service.GetById(id);
            if (viewmodel == null)
            {
                return NotFound();
            }

            _service.Delete(viewmodel);

            return viewmodel;
        }
    }
}
