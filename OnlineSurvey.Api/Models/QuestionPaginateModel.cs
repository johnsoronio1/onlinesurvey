﻿using OnlineSurvey.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineSurvey.Api.Models
{
    public class QuestionPaginateModel : PaginatedModel
    {
        public int? SurveyId { get; set; }
    }
}
