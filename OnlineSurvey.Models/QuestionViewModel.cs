﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineSurvey.Models
{
    public class QuestionViewModel : BaseModel
    {
        public QuestionViewModel() 
        {

        }

        public int SurveyId { get; set; }

        public string SurveyTitle { get; set; }

        [Required]
        [DisplayName("Display Text")]
        public string DisplayText { get; set; }
        [Required]
        public string Description { get; set; }
        [DisplayName("Required?")]
        public bool IsRequired { get; set; }
        [DisplayName("Required Message")]
        public string RequiredMessage { get; set; }
        [DisplayName("Minimum Int Value")]
        public int? MinIntValue { get; set; }
        [DisplayName("Maximum Int Value")]
        public int? MaxIntValue { get; set; }
        [DisplayName("Minimum Decimal Value")]
        public decimal? MinDecimalValue { get; set; }
        [DisplayName("Maximum Decimal Value")]
        public decimal? MaxDecimalValue { get; set; }
    }
}
