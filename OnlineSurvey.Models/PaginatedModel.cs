﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineSurvey.Models
{
    public class PaginatedModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SortField { get; set; }
        public string SortBy { get; set; }
        public List<PaginatedFilters> Filters { get; set; }
    }

    public class PaginatedFilters 
    { 
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
