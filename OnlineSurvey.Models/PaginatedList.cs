﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineSurvey.Models
{
    public abstract class PagedResultBase
    {
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int TotalRows { get; set; }

        public bool HasPrevious 
        {
            get { return PageNumber > 1; }
        }

        public bool HasNext 
        {
            get {  return PageNumber < TotalRows; }
        }

        public int FirstRowOnPage
        {
            get { return (PageNumber - 1) * PageSize + 1; }
        }

        public int LastRowOnPage
        {
            get { return Math.Min(PageNumber * PageSize, TotalRows); }
        }
    }

    public class PaginatedList<T> : PagedResultBase where T : class
    {
        public List<T> Results { get; set; }

        public PaginatedList()
        {
            Results = new List<T>();
        }
    }
}
