﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace OnlineSurvey.Models
{
    public class BaseModel
    {
        public int Id { get; set; }
        [DisplayName("Created Date")]
        public DateTime? DateCreated { get; set; }
        [DisplayName("Created By")]
        public int? DateCreatedBy { get; set; }
        [DisplayName("Updated Date")]
        public DateTime? DateUpdated { get; set; }
        [DisplayName("Updated By")]
        public int? DateUpdatedBy { get; set; }
    }
}
