﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OnlineSurvey.Models
{
    public class SurveyViewModel : BaseModel
    {
        public SurveyViewModel() { 
        
        }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public string Status { get; set; }
        [DisplayName("Date Published")]
        public DateTime? DatePublished { get; set; }
    }
}
