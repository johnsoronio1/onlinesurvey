﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineSurvey.Entities
{
    public interface IBaseEntity : IBaseEntity<int>
    {

    }

    public interface IBaseEntity<TId> where TId : IComparable
    {
        TId Id { get; set; }
        DateTime? DateCreated { get; set; }
        int? DateCreatedBy { get; set; }
        DateTime? DateUpdated { get; set; }
        int? DateUpdatedBy { get; set; }
    }
}
