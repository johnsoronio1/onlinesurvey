﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OnlineSurvey.Entities
{
    [Table("Survey")]
    public class Survey : IBaseEntity
    {
        public Survey() 
        {
            QuestionList = new List<Question>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime? DatePublished { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }

        public IList<Question> QuestionList { get; set; }
    }
}
