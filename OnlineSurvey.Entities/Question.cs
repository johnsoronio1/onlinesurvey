﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OnlineSurvey.Entities
{
    [Table("Question")]
    public class Question : IBaseEntity
    {
        public Question() 
        {
            Survey = new Survey();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Survey Survey { get; set; }
        [Required]
        public string DisplayText { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public string RequiredMessage { get; set; }
        public int? MinIntValue { get; set; }
        public int? MaxIntValue { get; set; }
        public decimal? MinDecimalValue { get; set; }
        public decimal? MaxDecimalValue { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? DateCreatedBy { get; set; }
        public DateTime? DateUpdated { get; set; }
        public int? DateUpdatedBy { get; set; }
    }
}
