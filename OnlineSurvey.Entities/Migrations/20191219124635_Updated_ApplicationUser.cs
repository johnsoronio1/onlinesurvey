﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineSurvey.Entities.Migrations
{
    public partial class Updated_ApplicationUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "cd01b1be-8b91-4f6d-935e-2e1c62442f45");

            migrationBuilder.DropColumn(
                name: "FullName",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "b4a58241-94aa-4b44-86e9-d1c204a4be7f", "5f97e8c3-4445-48fb-a4c9-49e7351e4629", "consumer", "CONSUMER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "IdentityRole",
                keyColumn: "Id",
                keyValue: "b4a58241-94aa-4b44-86e9-d1c204a4be7f");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "IdentityRole",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "cd01b1be-8b91-4f6d-935e-2e1c62442f45", "eff499d8-297f-4b16-be86-6103dd84a89f", "consumer", "CONSUMER" });
        }
    }
}
