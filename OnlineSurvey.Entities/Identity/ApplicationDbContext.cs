﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using OnlineSurvey.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineSurvey.Entities.Identity
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public string CurrentUserId { get; set; }

        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<LoginUser> LoginUsers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            modelBuilder.Entity<IdentityRole>().HasData(new IdentityRole { Name = OnlineSurvey.Entities.Constant.Roles.Consumer, NormalizedName = OnlineSurvey.Entities.Constant.Roles.Consumer.ToUpper() });
        }

        //public override int SaveChanges()
        //{
        //    var entities = ChangeTracker.Entries().Where(x => x.Entity is IBaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

        //    foreach (var entity in entities)
        //    {
        //        if (entity.State == EntityState.Added)
        //        {
        //            ((IBaseEntity)entity.Entity).DateCreated = DateTime.Now;
        //        }
        //        else 
        //        {
        //            ((IBaseEntity)entity.Entity).DateUpdated = DateTime.Now;
        //        }
        //    }

        //    return base.SaveChanges();
        //}
    }
}
