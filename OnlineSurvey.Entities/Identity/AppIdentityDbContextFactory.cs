﻿using Microsoft.EntityFrameworkCore;
using OnlineSurvey.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineSurvey.Entities.Identity
{
    public class AppIdentityDbContextFactory : DesignTimeDbContextFactoryBase<ApplicationDbContext>
    {
        protected override ApplicationDbContext CreateNewInstance(DbContextOptions<ApplicationDbContext> options)
        {
            return new ApplicationDbContext(options);
        }
    }
}
